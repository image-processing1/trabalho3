import os
import sys
from skimage import io
from scipy import ndimage
import numpy as np
import matplotlib.pyplot as plt
import cv2


def get_fft(img):
    freq_img = np.fft.fft2(img)
    return np.fft.fftshift(freq_img) #shift zero-frequence to center


def get_img(freqs):
    img = np.fft.ifftshift(freqs)
    img = np.fft.ifft2(img)
    return np.abs(img)


def get_magnitude(freqs):
    return 20*np.log(np.abs(freqs))


def get_lowpass_filter(shape, radius):
    filter = np.zeros(shape)
    filter = cv2.circle(filter, (round(shape[0]/2), round(shape[1]/2)),
        radius, (1,1,1), -1)
    return filter


def get_highpass_filter(shape, radius):
    filter = np.ones(shape)
    filter = cv2.circle(filter, (round(shape[0]/2), round(shape[1]/2)),
        radius, (0,0,0), -1)
    return filter


def get_bandpass_filter(shape, inside_radius, outside_radius):
    filter = np.zeros(shape)
    filter = cv2.circle(filter, (round(shape[0]/2), round(shape[1]/2)),
        outside_radius, (1,1,1), -1)
    filter = cv2.circle(filter, (round(shape[0]/2), round(shape[1]/2)),
        inside_radius, (0,0,0), -1)
    return filter


def compress_img(freqs, percent, mode='percentile'):
    if(mode=='percentile'):
        th = np.percentile(get_magnitude(freqs), percent)
    elif(mode=='max'):
        th = np.max(get_magnitude(freqs))*percent/100

    return np.where(get_magnitude(freqs) < th, 0, freqs)


if __name__ == '__main__':
    assert len(sys.argv)==2, "You should use image path as first argument"
    path = sys.argv[1]
    img = io.imread(path)
    print("Shape: ", img.shape, " Type: ", img.dtype)
    os.makedirs('results/',exist_ok=True)

    img=img/255
    #get spectrum
    freq_img = get_fft(img)

    #lowpass filter
    lpfilter = get_lowpass_filter(img.shape, 50)
    #highpass filter
    hpfilter = get_highpass_filter(img.shape, 50)
    #bandpass filter
    bpfilter = get_bandpass_filter(img.shape, 50, 100)

    #aplica filtros
    #lowpass
    freq_lp_filt = freq_img * lpfilter
    lp_img = get_img(freq_lp_filt)
    #highpass
    freq_hp_filt = freq_img * hpfilter
    hp_img = get_img(freq_hp_filt)
    #bandpass
    freq_bp_filt = freq_img * bpfilter
    bp_img = get_img(freq_bp_filt)


    #----------SAVING IMAGES---------------
    io.imsave('results/img_mag.png', get_magnitude(freq_img))
    io.imsave('results/img_inverse.png', get_img(freq_img))
    io.imsave('results/kernel_low_pass.png', np.clip(get_magnitude(freq_img*lpfilter),0,255))
    io.imsave('results/kernel_high_pass.png',  np.clip(get_magnitude(freq_img*hpfilter),0,255))
    io.imsave('results/kernel_band_pass.png', np.clip(get_magnitude(freq_img*bpfilter),0,255))
    io.imsave('results/img_low_pass.png', lp_img)
    io.imsave('results/img_high_pass.png', hp_img)
    io.imsave('results/img_band_pass.png', bp_img)

    #--------------- COMPRESS AND SAVE ----------------
    compressed_freq = compress_img(freq_img, 95, 'percentile')
    compressed_img = get_img(compressed_freq)
    io.imsave('results/compressed_percent_95.png', compressed_img)
    io.imsave('results/magnitude_compressed95.png', np.clip(get_magnitude(compressed_freq),0,255))

    compressed_freq = compress_img(freq_img, 99, 'percentile')
    compressed_img = get_img(compressed_freq)
    io.imsave('results/compressed_percent_99.png', compressed_img)
    io.imsave('results/magnitude_compressed99.png', np.clip(get_magnitude(compressed_freq),0,255))


    compressed_freq = compress_img(freq_img, 99.9, 'percentile')
    compressed_img = get_img(compressed_freq)
    io.imsave('results/compressed_percent_99.9.png', compressed_img)
    io.imsave('results/magnitude_compressed99.9.png', np.clip(get_magnitude(compressed_freq),0,255))
